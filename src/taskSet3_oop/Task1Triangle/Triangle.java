package taskSet3_oop.Task1Triangle;

public class Triangle {

        /*
        Task:
        Utwórz klasę Triangle i napisz metodę isRectangular(), która jako argumenty przyjmować będzie trzy liczby
        całkowite. Metoda powinna zwrócić true jeśli z odcinków o długości przekazanych w argumentach można zbudować
        trójkąt prostokątny. Wzór który może pomóc: c2 = a2 + b2
         */

        double a;
        double b;
        double c;

        public Triangle (double side1, double side2, double side3){
            a = side1;
            b = side2;
            c = side3;
        }

        public String isRectangular(){
            if (c*c == a*a + b*b){
                return "===============================\nThe triangle is rectangular\n==============================";
            }return "===============================\nThe triangle is not rectangular\n===============================";
        }
}
