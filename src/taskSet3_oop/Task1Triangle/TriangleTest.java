package taskSet3_oop.Task1Triangle;

import java.util.Scanner;

public class TriangleTest {

    public static void main(String[] args) {

        double userInput1;
        double userInput2;
        double userInput3;

        Scanner scan= new Scanner(System.in);

        // ask for number 1, check if input is a number, if not a number try again
        while (true) {
            System.out.println("Please enter the value of side 1 (a): ");
            try {
                userInput2= Double.parseDouble(scan.next());
                break;
            } catch (NumberFormatException ignore) {
                System.out.println("This is not a number, please try again: ");
            }
        }

        // ask for number 1, check if input is a number, if not a number try again
        while (true) {
            System.out.println("Please enter the value of side 2 (b): ");
            try {
                userInput3= Double.parseDouble(scan.next());
                break;
            } catch (NumberFormatException ignore) {
                System.out.println("This is not a number, please try again: ");
            }
        }

        // ask for number 1, check if input is a number, if not a number try again
        while (true) {
            System.out.println("Please enter the value of side 3 (c): ");
            try {
                userInput1= Double.parseDouble(scan.next());
                break;
            } catch (NumberFormatException ignore) {
                System.out.println("This is not a number, please try again: ");
            }
        }

        Triangle testTriangle = new Triangle(userInput1, userInput2, userInput3);

        System.out.println(testTriangle.isRectangular());

    }
}
