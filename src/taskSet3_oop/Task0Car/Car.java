package taskSet3_oop.Task0Car;

public class Car {

    String brand;
    String color;

    public Car(String brand, String color) {
        this.brand = brand;
        this.color = color;
    }

    @Override
    public String toString() {
        return color + " " + brand;
    }
}
