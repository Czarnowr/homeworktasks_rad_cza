package taskSet3_oop.task4Temperature;

public class Temperature {

    private int temperature;
    private String date;
    private String hour;

    public Temperature(int temperature, String date, String hour) {
        this.temperature = temperature;
        this.date = date;
        this.hour = hour;
    }

    public int getTemperature() {
        return temperature;
    }

    public String getDate() {
        return date;
    }

    public String getHour() {
        return hour;
    }

    public void show (){
        System.out.println(date + " " + hour + " - " + temperature + "°C");
    }

}
