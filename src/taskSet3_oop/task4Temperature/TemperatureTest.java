package taskSet3_oop.task4Temperature;

import taskSet3_oop.task2FahrenheitConverter.FahrenheitConverter;

public class TemperatureTest {
    public static void main(String[] args) {

        int tempInput = 10;
        String dateInput = "24.10.2018";
        String hourInput = "10:23";

        Temperature test = new Temperature(tempInput, dateInput, hourInput);
        test.show();

        double celTofar;
        FahrenheitConverter converter = new FahrenheitConverter();
        celTofar = converter.celsiusToFahrenheit(tempInput);
        System.out.println(tempInput + "°C equals " + celTofar + "°F");
    }
}
