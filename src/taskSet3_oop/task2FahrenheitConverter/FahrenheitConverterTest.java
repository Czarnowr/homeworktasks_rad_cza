package taskSet3_oop.task2FahrenheitConverter;


// imported for formatting

import java.text.DecimalFormat;
import java.util.Scanner;

// imported for scanner

public class FahrenheitConverterTest {

    public static void main(String[] args) {


        /*
        Task 7
        W osobnej klasie FahrenheitConverter, w metodzie main() napisz program przekształcający dane
        o temperaturze podanej w skali Fahrenheit do skali Celsjusza. Dane wejściowe (temperatura w
        skali Fahrenheit) podać w inicjacji odpowiedniej zmiennej w programie. Sprawdź czy program
        poprawnie oblicza temperatury dla danych: 32 °F = 0 °C; 212 °F = 100 °C
         */

        // display task name
        System.out.println();
        System.out.println("Converting with a scanner:");
        System.out.println();


        FahrenheitConverter converter = new FahrenheitConverter();

        // variables
        String choice;
        double numOfCelsius;
        double numOfFahrenheit;
        double celToFar;
        double farToCel;


        // initiate scanner
        Scanner scan = new Scanner(System.in);


        // Scanner with extra levels of security:
        // first: checks for a/A or b/B (choice of function)
        // if input is correct - the next step is executed, if not - the step is repeated until correct input
        // second: in each function checks for correct input (double), if not correct the step is repeated
        // the loop only ends after all inputs were correct

        boolean readyToExit = false;
        while (!readyToExit) {
            // ask for a letter representing the direction of change
            System.out.println("Choose one: \n A: Celsius to Fahrenheit \n B: Fahrenheit to Celsius");
            choice = scan.next();


            // if 'a' or 'A' was entered: execute Celsius to Fahrenheit converter.
            if (choice.toUpperCase().equals("A")) {
                System.out.println("Enter the temperature in Celsius:");
                while (true) {
                    try {
                        numOfCelsius = Double.parseDouble(scan.next());
                        break;
                    } catch (NumberFormatException ignore) {
                        System.out.println("This is not a number, please try again: ");
                    }
                }
                celToFar = converter.celsiusToFahrenheit(numOfCelsius);

                // changing the input from the user into String and checking the length of the Strings
                String sNumOfCelsius = Double.toString(numOfCelsius);
                int sNumOfCelsiusLength = sNumOfCelsius.length();
                //checking the number of decimal places in the Strings representing input
                int sNumOfCelsiusInt = sNumOfCelsius.indexOf('.');
                int sNumOfCelsiusDec = sNumOfCelsiusLength - sNumOfCelsiusInt - 1;


                DecimalFormat decAccu = new DecimalFormat();
                decAccu.setMaximumFractionDigits(sNumOfCelsiusDec);

                System.out.println();
                System.out.println(decAccu.format(numOfCelsius) + " Celsius equals " + decAccu.format(celToFar) + " Fahrenheit");
                readyToExit = true;


                // if 'b' or 'B' was entered: execute Fahrenheit to Celsius converter.
            } else if (choice.toUpperCase().equals("B")) {
                System.out.println("Enter the temperature in Fahrenheit:");

                while (true) {
                    try {
                        numOfFahrenheit = Double.parseDouble(scan.next());
                        break;
                    } catch (NumberFormatException ignore) {
                        System.out.println("This is not a number, please try again: ");
                    }
                }
                farToCel = converter.fahrenheitToCelsius(numOfFahrenheit);

                // changing the input from the user into String and checking the length of the Strings
                String sNumOfFahrenheit = Double.toString(numOfFahrenheit);
                int sNumOfFahrenheitLength = sNumOfFahrenheit.length();
                //checking the number of decimal places in the Strings representing input
                int sNumOfFahrenheitInt = sNumOfFahrenheit.indexOf('.');
                int sNumOfFahrenheitDec = sNumOfFahrenheitLength - sNumOfFahrenheitInt - 1;


                DecimalFormat decAccu = new DecimalFormat();
                decAccu.setMaximumFractionDigits(sNumOfFahrenheitDec);


                System.out.println();
                System.out.println(decAccu.format(numOfFahrenheit) + " Fahrenheit equals " + decAccu.format(farToCel) + " Celsius");
                readyToExit = true;
            } else {
                System.out.println("Try again.");
            }
        }

    }

}
