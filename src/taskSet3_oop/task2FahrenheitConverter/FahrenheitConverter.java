package taskSet3_oop.task2FahrenheitConverter;

public class FahrenheitConverter {

    // calculation for changing the temperature from Celsius to Fahrenheit
    public double celsiusToFahrenheit(double celsius){
        return ((celsius * 9 / 5) + 32);
    }


    // calculation for changing the temperature from Fahrenheit to Celsius
    public double fahrenheitToCelsius(double fahrenheit) {
        return ((fahrenheit - 32) * 5 / 9);
    }
}
