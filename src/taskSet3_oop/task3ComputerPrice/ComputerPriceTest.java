package taskSet3_oop.task3ComputerPrice;

// imported for Scanner

import java.util.Scanner;

public class ComputerPriceTest {

    public static void main(String[] args) {

        double mainBoardPrice;   // Main Board
        double processorPrice;   // Processor
        double ramPrice;         // RAM
        double hardDrivePrice;   // Hard Drive
        double screenPrice;      // Screen

        // initiate Scanner
        Scanner scan = new Scanner(System.in);

        //Header
        System.out.println("Please enter the following prices:\n========================================================");

        // Ask for the mainBoardPrice of the main board
        // ask for a number, check if input is a number, if not a number try again
        while (true) {
            System.out.print("Please enter the mainBoardPrice of the Processor: ");
            try {
                mainBoardPrice = Double.parseDouble(scan.next());
                break;
            } catch (NumberFormatException ignore) {
                System.out.println("This is not a number, please try again: ");
            }
        }

        // ask for a number, check if input is a number, if not a number try again
        while (true) {
            System.out.print("Please enter the mainBoardPrice of the Processor: ");
            try {
                processorPrice = Double.parseDouble(scan.next());
                break;
            } catch (NumberFormatException ignore) {
                System.out.println("This is not a number, please try again: ");
            }
        }

        // Ask for the mainBoardPrice of the RAM
        // ask for a number, check if input is a number, if not a number try again
        while (true) {
            System.out.print("Please enter the mainBoardPrice of the RAM: ");
            try {
                ramPrice = Double.parseDouble(scan.next());
                break;
            } catch (NumberFormatException ignore) {
                System.out.println("This is not a number, please try again: ");
            }
        }

        // Ask for the mainBoardPrice of the hard drive
        // ask for a number, check if input is a number, if not a number try again
        while (true) {
            System.out.print("Please enter the mainBoardPrice of the hard drive: ");
            try {
                hardDrivePrice = Double.parseDouble(scan.next());
                break;
            } catch (NumberFormatException ignore) {
                System.out.println("This is not a number, please try again: ");
            }
        }

        // Ask for the mainBoardPrice of the Screen
        // ask for a number, check if input is a number, if not a number try again
        while (true) {
            System.out.print("Please enter the mainBoardPrice of the Screen: ");
            try {
                screenPrice = Double.parseDouble(scan.next());
                break;
            } catch (NumberFormatException ignore) {
                System.out.println("This is not a number, please try again: ");
            }
        }

        ComputerPrice showComputerPrice = new ComputerPrice();
        System.out.println("Computer mainBoardPrice without VAT is: " + showComputerPrice.getComputerPrice(mainBoardPrice, processorPrice, ramPrice, hardDrivePrice));
        System.out.println("Monitor mainBoardPrice without VAT is: " + showComputerPrice.getMonitorPrice(screenPrice));
        System.out.println("Computer and monitor mainBoardPrice without VAT is: " + showComputerPrice.getComputerAndMonitorPrice(mainBoardPrice, processorPrice, ramPrice, hardDrivePrice, screenPrice));
        System.out.println("=================================================");
        System.out.println("Computer mainBoardPrice without VAT is: " + showComputerPrice.getComputerPriceWithVat(mainBoardPrice, processorPrice, ramPrice, hardDrivePrice));
        System.out.println("Monitor mainBoardPrice without VAT is: " + showComputerPrice.getMonitorPriceWithVat(screenPrice));
        System.out.println("Computer and monitor mainBoardPrice without VAT is: " + showComputerPrice.getComputerAndMonitorPriceWithVat(mainBoardPrice, processorPrice, ramPrice, hardDrivePrice, screenPrice));
    }
}
