package taskSet3_oop.task3ComputerPrice;

public class ComputerPrice {

    private double vat = 0.23;

    public double getComputerPrice(double mainBoardPrice, double processorPrice, double ramPrice, double hardDrivePrice) {
        return (mainBoardPrice + processorPrice + ramPrice + hardDrivePrice);
    }

    public double getMonitorPrice(double screenPrice) {
        return (screenPrice);
    }

    public double getComputerAndMonitorPrice(double mainBoardPrice, double processorPrice, double ramPrice, double hardDrivePrice, double screenPrice) {
        return (getComputerPrice(mainBoardPrice, processorPrice, ramPrice, hardDrivePrice) + getMonitorPrice(screenPrice));
    }

    public double getComputerPriceWithVat(double mainBoardPrice, double processorPrice, double ramPrice, double hardDrivePrice) {
        return ((mainBoardPrice + processorPrice + ramPrice + hardDrivePrice)+((mainBoardPrice + processorPrice + ramPrice + hardDrivePrice)*vat));
    }

    public double getMonitorPriceWithVat(double screenPrice) {
        return ((screenPrice) + (screenPrice*vat));
    }

    public double getComputerAndMonitorPriceWithVat(double mainBoardPrice, double processorPrice, double ramPrice, double hardDrivePrice, double screenPrice) {
        return ((getComputerPrice(mainBoardPrice, processorPrice, ramPrice, hardDrivePrice) + getMonitorPrice(screenPrice)) + ((getComputerPrice(mainBoardPrice, processorPrice, ramPrice, hardDrivePrice) + getMonitorPrice(screenPrice))*vat));
    }
}
