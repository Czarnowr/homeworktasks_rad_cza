package taskSet2_statements;

// imported for Scanner

import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {

        /*
        Task 5
        Napisz program, który dla podanej liczby wypisze na ekran konsoli dzień tygodnia
        (dla 1 - "poniedziałek", 2 - "wtorek" itp). Dodatkowo wyświetl ile dni zostało do weekendu, dla poniedziałku -
        5 dni, wtorku - 4 itp
        */


        // display task name
        System.out.println("");
        System.out.println("Task 5");
        System.out.println("");


        // values
        int task5DayNum;


        // initiate Scanner
        Scanner scan = new Scanner(System.in);


        boolean readyToExit = false;
        while (!readyToExit) {

            // ask for a grade, check if integer
            while (true) {
                System.out.println("Enter a grade between 1 and 7");
                try {
                    task5DayNum = Integer.parseInt(scan.next());
                    break;
                } catch (NumberFormatException e) {
                    System.out.println("This is not an integer, please try again: ");
                }
            }

            // if between 1 and 7 - continue
            if (task5DayNum >= 1 && task5DayNum <= 7) {


                // switch used to display which day the number represents
                switch (task5DayNum) {
                    case 1:
                        System.out.println("Day nr 1 is Monday.");
                        break;
                    case 2:
                        System.out.println("Day nr 2 is Tuesday.");
                        break;
                    case 3:
                        System.out.println("Day nr 3 is Wednesday.");
                        break;
                    case 4:
                        System.out.println("Day nr 4 is Thursday.");
                        break;
                    case 5:
                        System.out.println("Day nr 5 is Friday.");
                        break;
                    case 6:
                        System.out.println("Day nr 6 is Saturday.");
                        break;
                    case 7:
                        System.out.println("Day nr 7 is Sunday.");
                        break;
                }


                // Calculation of time remaining until weekend
                if ((task5DayNum >= 1) && (task5DayNum <= 5)) {
                    System.out.println("Days until weekend: " + (6 - task5DayNum));
                } else if ((task5DayNum >= 6) && (task5DayNum <= 7)) {
                    System.out.println("The weekend is now! Days until next weekend: " + (14 - task5DayNum));
                }

                readyToExit = true;

                // if not between 1 and 7 - loop back
            } else {
                System.out.println("Try again.");
            }
        }
    }
}
