package taskSet2_statements;

// imported for Scanner
import java.util.Scanner;

public class Task1 {

    public static void main(String[] args) {

         /*
        Task 1
        Napisz program, który wypisze na ekran konsoli, czy dana liczba całkowita znajduje się w przedziale
        1-10, 11-100, 101-1000, 1001-10000, czy też może jest mniejsza od 0 lub większa od 10000. Parametrem
        wejściowym niech będzie zmienna zainicjowana na początku programu.
         */

        // display task name
        System.out.println("");
        System.out.println("Task 1");
        System.out.println("");

        // variable
        int task1Num1;


        // Initiate Scanner
        Scanner scan = new Scanner(System.in);


        // ask for number 1, check if input is an integer, if not an integer try again
        while (true) {
            System.out.println("Please enter an integer: ");
            try {
                task1Num1 = Integer.parseInt(scan.next());
                break;
            } catch (NumberFormatException ignore) {
                System.out.println("This is not an integer, please try again: ");
            }
        }

        // if statement checking which range the number belongs to
        if (task1Num1 > 10000) {
            System.out.println(task1Num1 + " is higher than 10,000");
        } else if (task1Num1 > 1000) {
            System.out.println(task1Num1 + " is between 1,001 and 10,000");
        } else if (task1Num1 > 100) {
            System.out.println(task1Num1 + " is between 101 and 1,000");
        } else if (task1Num1 > 10) {
            System.out.println(task1Num1 + " is between 11 and 100");
        } else if (task1Num1 > 0) {
            System.out.println(task1Num1 + " is between 1 and 10");
        } else {
            System.out.println(task1Num1 + " is lower than 0");
        }


    }
}
