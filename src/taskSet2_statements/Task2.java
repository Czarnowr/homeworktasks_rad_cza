package taskSet2_statements;

// imported for Scanner

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {

         /*
        Task 2
        Napisz program, który wypisze na ekran konsoli, słowo oznaczające ocenę dla podanej cyfry.
        Np. dla 1 - "niedostateczny", 2 - "mierny" itp. Obsłuż przypadek gdy cyfra jest poza skalą ocen.
         */

//        Grades in the English system:
//        A = wzorowy
//        B = bardzo dobry
//        C = dobry
//        D = dostateczny
//        E = dopuszczajy
//        F = niedostateczny


        // display task name
        System.out.println("");
        System.out.println("Task 2");
        System.out.println("");


        // variable
        int gradeInput;


        // initiate Scanner
        Scanner scan = new Scanner(System.in);


        // Scanner with extra levels of security:
        // first: checks if input is an integer, if not - repeat
        // second: checks if input is between 1 and 6, if not - repeat
        // the loop only ends after all inputs were correct

        boolean readyToExit = false;
        while (!readyToExit) {

            // ask for a grade, check if integer
            while (true) {
                System.out.println("Enter a grade between 1 and 6");
                try {
                    gradeInput = Integer.parseInt(scan.next());
                    break;
                } catch (NumberFormatException e) {
                    System.out.println("This is not an integer, please try again: ");
                }
            }

            // if between 1 and 6 - continue
            if (gradeInput >= 1 && gradeInput <= 6) {

                System.out.println();
                System.out.println("IF method:");
                System.out.println("Polish grade = " + gradeInput);


                // IF method
                if (gradeInput == 1) {
                    System.out.println("English grade = F");
                } else if (gradeInput == 2) {
                    System.out.println("English grade = E");
                } else if (gradeInput == 3) {
                    System.out.println("English grade = D");
                } else if (gradeInput == 4) {
                    System.out.println("English grade = C");
                } else if (gradeInput == 5) {
                    System.out.println("English grade = B");
                } else if (gradeInput == 6) {
                    System.out.println("English grade = A");
                }


                System.out.println("");
                System.out.println("SWITCH method:");
                System.out.println("Polish grade = " + gradeInput);

                // SWITCH method
                switch (gradeInput) {
                    case 1:
                        System.out.println("English grade = F");
                        break;
                    case 2:
                        System.out.println("English grade = E");
                        break;
                    case 3:
                        System.out.println("English grade = D");
                        break;
                    case 4:
                        System.out.println("English grade = C");
                        break;
                    case 5:
                        System.out.println("English grade = B");
                        break;
                    case 6:
                        System.out.println("English grade = A");
                        break;
                }

                readyToExit = true;

                // if not between 1 and 6 - loop back
            } else {
                System.out.println("Try again.");
            }
        }
    }
}



