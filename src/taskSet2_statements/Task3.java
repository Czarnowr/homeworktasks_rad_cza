package taskSet2_statements;

// imported for Scanner
import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {


        /*
        Task 3
        Napisz program, który wypisze na ekran konsoli, cyfrę arabską dla podanej liczby rzymskiej (od 1 do 9).
        Czyli np. dla 'I' wypisze 1, dla 'V' 5 itp. Obsłuż przypadek gdy podana liczba rzymska jest nieprawidłowa.
        */


        // display task name
        System.out.println("");
        System.out.println("Task 3");
        System.out.println("");


        // tool title
        System.out.println("Roman to Arabic numbers Converter");
        System.out.println("");


        // values
        String task3RomanNum;


        // initiate Scanner
        Scanner scan = new Scanner(System.in);
        System.out.println("Please enter a Roman number between I and X: ");
        task3RomanNum = scan.next();


        // changing the numbers using the 'IF' statement
        if (task3RomanNum.equals("I")) {
            System.out.println("I = 1");
        } else if (task3RomanNum.equals("II")) {
            System.out.println("II = 2");
        } else if (task3RomanNum.equals("III")) {
            System.out.println("III = 3");
        } else if (task3RomanNum.equals("IV")) {
            System.out.println("IV = 4");
        } else if (task3RomanNum.equals("V")) {
            System.out.println("V = 5");
        } else if (task3RomanNum.equals("VI")) {
            System.out.println("VI = 6");
        } else if (task3RomanNum.equals("VII")) {
            System.out.println("VII = 7");
        } else if (task3RomanNum.equals("VIII")) {
            System.out.println("VIII = 8");
        } else if (task3RomanNum.equals("IX")) {
            System.out.println("IX = 9");
        } else if (task3RomanNum.equals("X")) {
            System.out.println("X = 10");
        } else {
            System.out.println("This is not a Roman numeral between I and X");
        }

    }
}
