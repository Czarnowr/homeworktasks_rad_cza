package taskSet2_statements;

// imported for Scanner
import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {

        /*
        Task 4
        Napisz program, który wypisze na ekran konsoli czy podany kod Unicode jest liczbą (0-9), małą literą (a-z)
        czy też dużą literą (A-Z). Kody każdej z grup znaków następują po sobie więc wystarczy znaleźć kod np. dla
        litery 'a' i 'z' i sprawdzić czy podany kod zawiera się w tym przedziale.
        */


        // display task name
        System.out.println("");
        System.out.println("Task 4");
        System.out.println("");


        // value (integer, because of Scanner needs)
        int task4UniInt;


        // initiate Scanner
        Scanner scan = new Scanner(System.in);


        //Scanner receives an 'int' value and converts it to a 'char' value, as cannot scan for char.
        while (true) {
            System.out.println("Please enter a Unicode character number: ");
            try {
                task4UniInt = Integer.parseInt(scan.next());
                break;
            } catch (NumberFormatException ignore) {
                System.out.println("This is not a number, please try again: ");
            }
        }


        // value task4UniInt changed from 'int' to 'char'
        char task4UniChar = (char) task4UniInt;


        // checking if the char value is within the given value ranges
        if ((task4UniChar >= 97) && (task4UniChar <= 122)) {
            System.out.println("'" + task4UniChar + "' is a small letter");
        } else if ((task4UniChar >= 65) && (task4UniChar <= 90)) {
            System.out.println("'" + task4UniChar + "' is a capital letter");
        } else if ((task4UniChar >= 48) && (task4UniChar <= 57)) {
            System.out.println("'" + task4UniChar + "' is a number between 0 and 9");
        } else {
            System.out.println("This is not a small letter, capital letter, or a number between 0 and 9");
        }

    }
}
