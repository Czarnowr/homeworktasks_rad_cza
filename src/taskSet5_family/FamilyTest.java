package taskSet5_family;

public class FamilyTest {
    public static void main(String[] args) {

        Person sonRobertBrown = new Person("Robert", "Brown", 13);
        Person daughterLaraBrown = new Person("Lara", "Brown",17);
        Person fatherThomasBrown = new Person("Thomas", "Brown",40);
        Person motherBarbaraBrown = new Person("Barbara", "Brown",38);
        Person grandfatherEarlBrown = new Person("Earl", "Brown",60);
        Person grandmotherElisaBrown = new Person("Elisa", "Brown",59);
        Family familyBrown = new Family(sonRobertBrown, daughterLaraBrown, fatherThomasBrown, motherBarbaraBrown, grandfatherEarlBrown, grandmotherElisaBrown);

        System.out.println(familyBrown);
        System.out.println("\nCollective age: " + familyBrown.collectiveFamilyAge());
        System.out.println("Average age: " + familyBrown.averageFamilyAge());

    }
}
