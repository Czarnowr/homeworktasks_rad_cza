package taskSet5_family;

public class Person {
    private String firstName;
    private String surname;
    private int age;

    Person(String firstName, String surname, int age) {
        this.firstName = firstName;
        this.surname = surname;
        this.age = age;
    }

    public String getFirstName() {
        return firstName;
    }

    String getSurname() {
        return surname;
    }

    int getAge() {
        return age;
    }

    @Override
    public String toString() {
        return firstName + ' ' + surname + ", " + age;
    }
}
