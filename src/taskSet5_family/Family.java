package taskSet5_family;

public class Family {

    private Person son;
    private Person daughter;
    private Person father;
    private Person mother;
    private Person grandfather;
    private Person grandmother;
    private int familySize;

    Family(Person son, Person daughter, Person father, Person mother, Person grandfather, Person grandmother) {
        this.son = son;
        this.daughter = daughter;
        this.father = father;
        this.mother = mother;
        this.grandfather = grandfather;
        this.grandmother = grandmother;
        this.familySize = 6;
    }

    double collectiveFamilyAge (){
        return son.getAge() + daughter.getAge() + father.getAge() + mother.getAge() + grandmother.getAge() + grandfather.getAge();
    }

    double averageFamilyAge (){
        return (son.getAge() + daughter.getAge() + father.getAge() + mother.getAge() + grandmother.getAge() + grandfather.getAge())/familySize;
    }

    public Person getSon() {
        return son;
    }

    public Person getDaughter() {
        return daughter;
    }

    public Person getFather() {
        return father;
    }

    public Person getMother() {
        return mother;
    }

    public Person getGrandfather() {
        return grandfather;
    }

    public Person getGrandmother() {
        return grandmother;
    }

    @Override
    public String toString() {
        return "Family " + father.getSurname() + ":\n" +
                "\nSon: " + son +
                "\nDaughter: " + daughter +
                "\nFather: " + father +
                "\nMother: " + mother +
                "\nGrandfather: " + grandfather +
                "\nGrandmother: " + grandmother;
    }
}
