package taskSet1_datatypes.task7FahrenheitConverter;


// imported for formatting
import java.text.DecimalFormat;
// imported for scanner
import java.util.Scanner;

public class FahrenheitConverterTest {

    public static void main(String[] args) {


        /*
        Task 7
        W osobnej klasie FahrenheitConverter, w metodzie main() napisz program przekształcający dane
        o temperaturze podanej w skali Fahrenheit do skali Celsjusza. Dane wejściowe (temperatura w
        skali Fahrenheit) podać w inicjacji odpowiedniej zmiennej w programie. Sprawdź czy program
        poprawnie oblicza temperatury dla danych: 32 °F = 0 °C; 212 °F = 100 °C
         */

        // display version
        System.out.println();
        System.out.println("Converting manually:");
        System.out.println();

        // variables for test temperature values
        int fahrenheit32 = 32; // 32°F
        int fahrenheit212 = 212; // 212 °F
        int celsius0 = 0; // 0°C
        int celsius100 = 100; // 100°C


        // maths (magic) happens here
        int fahrenheit32toC = (fahrenheit32 - 32) * 5 / 9;
        int fahrenheit212toC = (fahrenheit212 - 32) * 5 / 9;
        int celsius0toF = (celsius0 * 9 / 5) + 32;
        int celsius100toF = (celsius100 * 9 / 5) + 32;


        // test for Fahrenheit 32
        System.out.println("Fahrenheit to Celsius:");
        System.out.println("Fahrenheit: " + fahrenheit32);
        System.out.println("Celsius: " + fahrenheit32toC);
        System.out.println("");
        // test for Fahrenheit 212
        System.out.println("Fahrenheit to Celsius:");
        System.out.println("Fahrenheit: " + fahrenheit212);
        System.out.println("Celsius: " + fahrenheit212toC);
        System.out.println("");
        // test for Celsius 0
        System.out.println("Celsius to Fahrenheit:");
        System.out.println("Celsius: " + celsius0);
        System.out.println("Fahrenheit: " + celsius0toF);
        System.out.println("");
        // test for Celsius 100
        System.out.println("Celsius to Fahrenheit:");
        System.out.println("Celsius: " + celsius100);
        System.out.println("Fahrenheit: " + celsius100toF);


    }

}
