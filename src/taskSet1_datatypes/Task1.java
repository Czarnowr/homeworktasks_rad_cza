package taskSet1_datatypes;

// imported for format editing
import java.text.DecimalFormat;
// imported for scanner
import java.util.Scanner;

public class Task1 {

    public static void main(String[] args) {

        /*
        Task 1
        Napisz program, który utwórzy dwie zmienne, a następnie wypisze na ekran ich sumę, różnicę, iloczyn i iloraz.
         */

        // display task name
        System.out.println("");
        System.out.println("Task 1");
        System.out.println("");

        // variables
        double task1Num1;
        double task1Num2;
        int decAccu;

        // initiate scanner
        Scanner scan = new Scanner(System.in);

        // ask for number 1, check if input is a number, if not a number try again
        while (true) {
            System.out.println("Please enter number 1: ");
            try {
                task1Num1 = Double.parseDouble(scan.next());
                break;
            } catch (NumberFormatException ignore) {
                System.out.println("This is not a number, please try again: ");
            }
        }

        // ask for number 2, check if input is a number, if not a number try again
        while (true) {
            System.out.println("Please enter number 2: ");
            try {
                task1Num2 = Double.parseDouble(scan.next());
                break;
            } catch (NumberFormatException ignore) {
                System.out.println("This is not a number, please try again: ");
            }
        }
        System.out.println();


        // ask for an integer (accuracy of the result), check if input is an integer, if not, try again
        while (true) {
            System.out.println("How accurate do you want the result to be? (How many decimals)");
            try {
                decAccu = Integer.parseInt(scan.next());
                break;
            } catch (NumberFormatException ignore) {
                System.out.println("This is not an integer, please try again!");
                System.out.println();
            }
        }
        System.out.println();



        // mathematics (magic) is done here
        double additionResult = task1Num1 + task1Num2;
        double subtractionResult = task1Num1 - task1Num2;
        double multiplicationResult = task1Num1 * task1Num2;
        double divisionResult = task1Num1 / task1Num2;


        // formatting of the results:


        // changing the input from the user into String and checking the length of the Strings
        String sTask1Num1 = Double.toString(task1Num1);
        int sTask1Num1CharNum = sTask1Num1.length();
        String sTask1Num2 = Double.toString(task1Num2);
        int sTask1Num2CharNum = sTask1Num2.length();

        //checking the number of decimal places in the Strings representing input
        int sTask1Num1int = sTask1Num1.indexOf('.');
        int sTask1Num1dec = sTask1Num1CharNum - sTask1Num1int - 1;
        int sTask1Num2int = sTask1Num2.indexOf('.');
        int sTask1Num2dec = sTask1Num2CharNum - sTask1Num2int - 1;

        // variable (result of comparison)
        int higherNumber;

        // comparing the length of the Strings above to get the number of decimal places to display
        if (sTask1Num1dec >= sTask1Num2dec) {
            higherNumber = sTask1Num1dec;
        } else {
            higherNumber = sTask1Num2dec;
        }

        // setting default formatting for input
        DecimalFormat df = new DecimalFormat();
        // set maximum number of decimal places. Example: 1.23456 = 1.23 (two places)
        df.setMaximumFractionDigits(higherNumber);


        // setting default formatting for result
        DecimalFormat dfResult = new DecimalFormat();
        // set maximum number of decimal places, selected by the user in the Scanner.
        dfResult.setMaximumFractionDigits(decAccu);


        // changing the variables (after receiving from Scanner) to new formatting
        String fTask1Num1 = df.format(task1Num1);
        String fTask1Num2 = df.format(task1Num2);


        // formatting of results (see above)
        String fAdditionResult = dfResult.format(additionResult);
        String fSubtractionResult = dfResult.format(subtractionResult);
        String fMultiplicationResult = dfResult.format(multiplicationResult);
        String fDivisionResult = dfResult.format(divisionResult);


        // presenting results of all four equations
        System.out.println("Addition of " + fTask1Num1 + " and " + fTask1Num2);
        System.out.println(fTask1Num1 + " + " + fTask1Num2 + " = " + fAdditionResult);
        System.out.println("");
        System.out.println("Subtraction of " + fTask1Num1 + " and " + fTask1Num2);
        System.out.println(fTask1Num1 + " - " + fTask1Num2 + " = " + fSubtractionResult);
        System.out.println("");
        System.out.println("Multiplication of " + fTask1Num1 + " and " + fTask1Num2);
        System.out.println(fTask1Num1 + " * " + fTask1Num2 + " = " + fMultiplicationResult);
        System.out.println("");
        // if statement replaces the equasion with a message if the second number is '0'
        System.out.println("Division of " + fTask1Num1 + " and " + fTask1Num2);
        if (task1Num2 == 0){
            System.out.println("You cannot divide by '0'!");
        }else{
            System.out.println(fTask1Num1 + " / " + fTask1Num2 + " = " + fDivisionResult);
        }
        System.out.println("");
    }
}