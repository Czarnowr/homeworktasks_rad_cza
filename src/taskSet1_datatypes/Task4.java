package taskSet1_datatypes;

// imported for format editing
import java.text.DecimalFormat;
// imported for scanner
import java.util.Scanner;

public class Task4 {

    public static void main(String[] args) {

        /*
        Task 4
        Napisz program, który tworzy jedną zmienną, a następnie wypisze jej wartość podniesioną do 3 potęgi.
         */

        // display task name
        System.out.println("");
        System.out.println("Task 4");
        System.out.println("");


        // variable
        double task4Num1;
        int decAccu;

        // initiate scanner
        Scanner scan = new Scanner(System.in);


        // ask for number 1, check if input is a number, if not a number try again
        while (true) {
            System.out.println("Please enter number 1: ");
            try {
                task4Num1 = Double.parseDouble(scan.next());
                break;
            } catch (NumberFormatException ignore) {
                System.out.println("This is not a number, please try again: ");
            }
        }

        // ask for an integer (accuracy of the result), check if input is an integer, if not, try again
        while (true) {
            System.out.println("How accurate do you want the result to be? (How many decimals)");
            try {
                decAccu = Integer.parseInt(scan.next());
                break;
            } catch (NumberFormatException ignore) {
                System.out.println("This is not an integer, please try again!");
                System.out.println();
            }
        }
        System.out.println();



        // formatting of the results



        // changing the input from the user into String and checking the length of the Strings
        String sTask1Num1 = Double.toString(task4Num1);
        int sTask4Num1CharNum = sTask1Num1.length();
        //checking the number of decimal places in the Strings representing input
        int sTask1Num1int = sTask1Num1.indexOf('.');
        int sTask1Num1dec = sTask4Num1CharNum - sTask1Num1int - 1;
        // setting default formatting for the input using the acquired decimal number
        DecimalFormat formatInput = new DecimalFormat();
        // the program will automatically match the decimal number with that of the input, but remove trailing 0s
        formatInput.setMaximumFractionDigits(sTask1Num1dec);

        // setting default formatting for the result
        DecimalFormat formatResult = new DecimalFormat();
        // the Scanner will ask for the number of decimals to be displayed in the result
        formatResult.setMaximumFractionDigits(decAccu);



        // mathematics (magic) is done here
        double task4Num1Cubic = Math.pow(task4Num1, 3);


        // The little character representing 'cubic'
        char cub3 = 179;


        //Printing the result to console
        System.out.println("The result of " + formatInput.format(task4Num1) + cub3 + " equals: " + formatResult.format(task4Num1Cubic));
    }
}
