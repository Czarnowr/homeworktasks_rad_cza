package taskSet1_datatypes.task8ComputerPrice;

// imported for Formatting
import java.text.DecimalFormat;
// imported for Scanner
import java.util.Scanner;

public class ComputerPrice {

    public static void main(String[] args) {


        /*
        Task 8
        W osobnej klasie ComputerPriceTest, w metodzie main() napisz program obliczający cenę komputera na podstawie
        jego części. Program ma wypisać na konsolę osobno cenę samego komputera: płyta główna, procesor, pamięć RAM,
        dysk twardy i osobno cenę komputera i monitora. W cenie należy uwzględnić podatek VAT = 23%.
         */


        double mainBoard;   // Main Board
        double processor;   // Processor
        double ram;         // RAM
        double hardDrive;   // Hard Drive
        double screen;      // Screen

        // initiate Scanner
        Scanner scan = new Scanner(System.in);

        //Header
        System.out.println("Please enter the following prices: ");
        System.out.println("");


        // Ask for the price of the main board
        // ask for a number, check if input is a number, if not a number try again
        while (true) {
            System.out.println("Please enter the price of the main board: ");
            try {
                mainBoard = Double.parseDouble(scan.next());
                break;
            } catch (NumberFormatException ignore) {
                System.out.println("This is not a number, please try again: ");
            }
        }

        // Ask for the price of the Processor
        // ask for a number, check if input is a number, if not a number try again
        while (true) {
            System.out.println("Please enter the price of the Processor: ");
            try {
                processor = Double.parseDouble(scan.next());
                break;
            } catch (NumberFormatException ignore) {
                System.out.println("This is not a number, please try again: ");
            }
        }

        // Ask for the price of the RAM
        // ask for a number, check if input is a number, if not a number try again
        while (true) {
            System.out.println("Please enter the price of the RAM: ");
            try {
                ram = Double.parseDouble(scan.next());
                break;
            } catch (NumberFormatException ignore) {
                System.out.println("This is not a number, please try again: ");
            }
        }

        // Ask for the price of the hard drive
        // ask for a number, check if input is a number, if not a number try again
        while (true) {
            System.out.println("Please enter the price of the hard drive: ");
            try {
                hardDrive = Double.parseDouble(scan.next());
                break;
            } catch (NumberFormatException ignore) {
                System.out.println("This is not a number, please try again: ");
            }
        }

        // Ask for the price of the Screen
        // ask for a number, check if input is a number, if not a number try again
        while (true) {
            System.out.println("Please enter the price of the Screen: ");
            try {
                screen = Double.parseDouble(scan.next());
                break;
            } catch (NumberFormatException ignore) {
                System.out.println("This is not a number, please try again: ");
            }
        }

        // price of computer
        double computer = (mainBoard + processor + ram + hardDrive);
        // VAT value
        double vat = (1.23);

        // setting default formatting for result
        DecimalFormat decAccu = new DecimalFormat();
        // set maximum number of decimal places, selected by the user in the Scanner.
        decAccu.setMaximumFractionDigits(2);

        // maths (magic) happens here:
        double mainBoardWithVAT = mainBoard * vat;
        double processorWithVAT = mainBoard * vat;
        double ramWithVAT = mainBoard * vat;
        double hardDriveWithVAT = mainBoard * vat;
        double computerWithVAT = mainBoard * vat;
        double screenWithVAT = mainBoard * vat;
        double computerAndScreenWithVAT = mainBoard * vat;

        // Print results to console:
        System.out.println("Main Board Price = " + "£" + mainBoard + "  (" + "£" + (decAccu.format(mainBoardWithVAT)) + " icl. VAT )");
        System.out.println("Processor Price = " + "£" + processor + "  (" + "£" + (decAccu.format(processorWithVAT)) + " icl. VAT )");
        System.out.println("RAM = " + "£" + ram + "  (" + "£" + (decAccu.format(ramWithVAT)) + " icl. VAT )");
        System.out.println("Hard Drive = " + "£" + hardDrive + "  (" + "£" + (decAccu.format(hardDriveWithVAT)) + " icl. VAT )");
        System.out.println("");
        System.out.println("Computer (all above)= " + "£" + computer + "  (" + "£" + (decAccu.format(computerWithVAT)) + " icl. VAT )");
        System.out.println("Screen = " + "£" + screen + "  (" + "£" + (decAccu.format(screenWithVAT)) + " icl. VAT )");
        System.out.println("");
        System.out.println("Overall (Computer + Screen)= " + "£" + (computer + screen) + "  (" + "£" + (decAccu.format(computerAndScreenWithVAT)) + " icl. VAT )");

    }

}
