package taskSet1_datatypes;

// imported for scanner

import java.util.Scanner;

public class Task5 {

    public static void main(String[] args) {

        /*
        Task 5
        Wyświetl na ekranie 5 pierwszych liter alfabetu łacińskiego (zaczyna się od kodu 65), hebrajskiego
        (zaczyna się od kodu 1488) i tybetańskiego (zaczyna się od kodu 3840).
         */


        // display task name
        System.out.println("");
        System.out.println("Task 5");
        System.out.println("");


        // set starting char values from the Unicode table
        char latinFiveChars = 65;
        char hebrewFiveChars = 1488;
        char tibetanFiveChars = 3840;


        // Version 1 //


        // display version
        System.out.println();
        System.out.println("Version 1:");
        System.out.println("Display the first 5 characters of each alphabet");
        System.out.println();


        //Printing the result to console (first 5 characters)
        System.out.println("First 5 Hebrew alphabet characters: " + latinFiveChars + ", " + ++latinFiveChars + ", " + ++latinFiveChars + ", " + ++latinFiveChars + ", " + ++latinFiveChars);
        System.out.println("");
        System.out.println("First 5 Hebrew alphabet characters: " + hebrewFiveChars + ", " + ++hebrewFiveChars + ", " + ++hebrewFiveChars + ", " + ++hebrewFiveChars + ", " + ++hebrewFiveChars);
        System.out.println("");
        System.out.println("First 5 Tibetan alphabet characters: " + tibetanFiveChars + ", " + ++tibetanFiveChars + ", " + ++tibetanFiveChars + ", " + ++tibetanFiveChars + ", " + ++tibetanFiveChars);
        System.out.println("");


        // Version 2 //


        // display version
        System.out.println();
        System.out.println("Version 2:");
        System.out.println();
        System.out.println("Ask how many characters to display and display those characters");
        System.out.println();


        // variables
        int task5Num1;
        int task5Num2;
        int task5Num3;

        // set starting char values from the Unicode table for the second version
        char latinStartPoint = 65;
        char hebrewStartPoint = 1488;
        char tibetanStartPoint = 3840;


        // initiate scanner
        Scanner scan = new Scanner(System.in);


        // ask for an integer, check if input is an integer, if not, try again - Latin
        while (true) {
            System.out.println("How many Latin characters do you want to see?");
            try {
                task5Num1 = Integer.parseInt(scan.next());
                break;
            } catch (NumberFormatException ignore) {
                System.out.println("This is not an integer, please try again!");
                System.out.println();
            }
        }
        System.out.println();


        // Latin alphabet:
        System.out.println("Latin alphabet characters:");


        // checks if the requested number of characters is smaller or equal to the number of letters in the alphabet.
        // if yes - prints the requested number of characters
        // if not - prints the whole alphabet, but not more
        int latinLimit = (latinStartPoint + task5Num1);

        if (latinLimit >= 1 && latinLimit < 91) {
            do {
                System.out.print(latinStartPoint + ", ");
            } while (++latinStartPoint < latinLimit);
        } else if (latinLimit >= 91) {
            do {
                System.out.print(latinStartPoint + ", ");
            } while (++latinStartPoint <= 90);
        }
        System.out.println("");


        // Hebrew alphabet:
        System.out.println("");
        System.out.println("Hebrew alphabet characters:");


        // ask for an integer, check if input is an integer, if not, try again - Hebrew
        while (true) {
            System.out.println("How many Hebrew characters do you want to see?");
            try {
                task5Num2 = Integer.parseInt(scan.next());
                break;
            } catch (NumberFormatException ignore) {
                System.out.println("This is not an integer, please try again!");
                System.out.println();
            }
        }
        System.out.println();


        // checks if the requested number of characters is smaller or equal to the number of letters in the alphabet.
        // if yes - prints the requested number of characters
        // if not - prints the whole alphabet, but not more
        int hebrewLimit = (hebrewStartPoint + task5Num2);

        if (hebrewLimit >= 1 && hebrewLimit < 1513) {
            do {
                System.out.print(hebrewStartPoint + ", ");
            } while (++hebrewStartPoint < hebrewLimit);
        } else if (hebrewLimit >= 1513) {
            do {
                System.out.print(hebrewStartPoint + ", ");
            } while (++hebrewStartPoint <= 1512);
        }
        System.out.println("");


        // Tibetan alphabet:
        System.out.println("");
        System.out.println("Tibetan alphabet characters:");


        // ask for an integer, check if input is an integer, if not, try again - Tibetan
        while (true) {
            System.out.println("How many Tibetan characters do you want to see?");
            try {
                task5Num3 = Integer.parseInt(scan.next());
                break;
            } catch (NumberFormatException ignore) {
                System.out.println("This is not an integer, please try again!");
                System.out.println();
            }
        }
        System.out.println();


        // checks if the requested number of characters is smaller or equal to the number of letters in the alphabet.
        // if yes - prints the requested number of characters
        // if not - prints the whole alphabet, but not more
        int tibetanLimit = (tibetanStartPoint + task5Num3);

        if (tibetanLimit >= 1 && tibetanLimit < 3872) {
            do {
                System.out.print(tibetanStartPoint + ", ");
            } while (++tibetanStartPoint < (tibetanLimit));
        } else if (tibetanLimit >= 3872) {
            do {
                System.out.print(tibetanStartPoint + ", ");
            } while (++tibetanStartPoint <= 3871
            );
        }


    }

}
