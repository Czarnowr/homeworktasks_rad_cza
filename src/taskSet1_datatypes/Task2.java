package taskSet1_datatypes;

// imported for scanner
import java.util.Scanner;

public class Task2 {

    public static void main(String[] args) {

        /*
        Task 2
        Napisz program, który utworzy jedną zmienną, a następnie wypisze na ekran czy wartość tej zmiennej jest
        liczbą parzystą czy niepoarzystą.
         */

        // display task name
        System.out.println("");
        System.out.println("Task 2");
        System.out.println("");


        // variables
        int task2Num1;


        // initiate scanner
        Scanner scan = new Scanner(System.in);


        // ask for an integer, check if input is an integer, if not, try again
        while (true) {
            System.out.println("Please enter an integer: ");
            try {
                task2Num1 = Integer.parseInt(scan.next());
                break;
            } catch (NumberFormatException ignore) {
                System.out.println("This is not an integer, please try again!");
                System.out.println();
            }
        }
        System.out.println();


        // display version number
        System.out.println("");
        System.out.println("Version 1: done using 'boolean' and 'if'");
        System.out.println("");


        // check if the input numbers can be divided by 2, save the reminder to the variable 'task2Num1' (% 2),
        // boolean + if: true = even number, false = not even number
        task2Num1 = task2Num1 % 2;
        boolean isTask2Num1Even = task2Num1 == 0;
        if (isTask2Num1Even) {
            System.out.println("'" + task2Num1 + "'" + " is an even number");
        } else {
            System.out.println("'" + task2Num1 + "'" + " is not an even number");
        }


        // display version number
        System.out.println("");
        System.out.println("Version 2:  done using 'if' statements only");
        System.out.println("");


        // check if the input numbers can be divided by 2, save the reminder to the variable 'task2Num1' (% 2),
        // if: if remainder = 0 the number is even, otherwise it is not
        task2Num1 = task2Num1 % 2;
        if (task2Num1 == 0)
            System.out.println("'" + task2Num1 + "'" + " is an even number");
        else
            System.out.println("'" + task2Num1 + "'" + " is not an even number");
    }
}
