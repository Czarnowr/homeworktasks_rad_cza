package taskSet1_datatypes;

// imported for scanner
import java.util.Scanner;

public class Task3 {

    public static void main(String[] args) {

        /*
        Task 3
        Napisz program, który utworzy jedną zmienną, a następnie wypisze na ekran czy wartość tej zmiennej jest
        podzielna przez 3 i jednocześnie przez 5.
         */


        // display task name
        System.out.println("");
        System.out.println("Task 3");
        System.out.println("");

        // variables
        int task3Num1;

        // initiate scanner
        Scanner scan = new Scanner(System.in);


        // ask for integer, check if input is an integer, if not, try again
        while (true) {
            System.out.println("Please enter an integer: ");
            try {
                task3Num1 = Integer.parseInt(scan.next());
                break;
            } catch (NumberFormatException ignore) {
                System.out.println("This is not an integer, please try again!");
                System.out.println();
            }
        }
        System.out.println();


        // check if the input numbers can be divided by 3, save the reminder to the variable 'task3Num1Div3' (% 3),
        double task3Num1Div3 = task3Num1 % 3;
        // check if the input numbers can be divided by 5, save the reminder to the variable 'task3Num1Div5' (% 5),
        double task3Num1Div5 = task3Num1 % 5;

        // compare the remainders to '0'
        // for every case, if '==0' then is dividable
        if (task3Num1Div3 == 0 && task3Num1Div5 == 0) {
            System.out.println("'" + task3Num1 + "'" + " is dividable by 3 and 5");
        } else if (task3Num1Div3 == 0) {
            System.out.println("'" + task3Num1 + "'" + " is dividable by 3, but not 5");
        } else if (task3Num1Div5 == 0) {
            System.out.println("'" + task3Num1 + "'" + " is dividable by 5, but not 3");
        } else
            System.out.println("'" + task3Num1 + "'" + " is not dividable by 3 or 5");
    }
}
