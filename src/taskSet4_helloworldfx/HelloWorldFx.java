package taskSet4_helloworldfx;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class HelloWorldFx extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {

        Label label1 = new Label("This is a changed label text");

        Button button1 = new Button("This is a changed button text");
        button1.setOnAction(e -> System.out.println("Button was clicked!"));

        Label label2 = new Label("This is a new label");

        Button button2 = new Button("This is a new button");
        button2.setOnAction(e -> System.out.println("Button was clicked!"));

        // inserted a text field
        TextField textField1 = new TextField();
        textField1.setPrefWidth(400);
        textField1.setPrefHeight(50);

        // inserted an extra label
        Label label3 = new Label("Text to be replaced");

        // inserted an extra button
        Button button3 = new Button("Copy text to label abo");
        // button moves the text from the text field to label3
        button3.setOnAction(e -> label3.setText(textField1.getText()));

        VBox box = new VBox();
        box.setAlignment(Pos.CENTER);
        box.getChildren().addAll(label1, button1, label2, button2, textField1, label3, button3);

        primaryStage.setTitle("Hello World - JavaFX");
        primaryStage.setScene(new Scene(box, 500, 200));
        primaryStage.show();
    }
}
