package taskSet6_encapsulation.task.item;

class ItemModel {
    private String title;
    private String description;
    private double price;

    ItemModel(String title, String description, double price) {
        this.title = title;
        this.description = description;
        this.price = price;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public double getPrice() {
        return price;
    }


    @Override
    public String toString() {
        return "Item: " +
                "Title = '" + title + '\'' +
                ", Description = '" + description + '\'' +
                ", Price = '" + price + "'";
    }
}
