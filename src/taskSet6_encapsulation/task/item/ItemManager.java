package taskSet6_encapsulation.task.item;

public class ItemManager {
    private ItemModel item1;

    public void createShopItem(String title, String description, double price) {
        item1 = new ItemModel(title, description, price);
    }

    public ItemModel getItem1() {
        return item1;
    }
}
