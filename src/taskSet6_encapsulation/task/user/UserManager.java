package taskSet6_encapsulation.task.user;

public class UserManager {
    private UserModel user1;

    public void createUser1(String firstName, String lastName, int age) {
        user1 = new UserModel(firstName, lastName, age);
    }

    public UserModel getUser1() {
        return user1;
    }

}
