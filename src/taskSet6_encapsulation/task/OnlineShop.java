package taskSet6_encapsulation.task;

import taskSet6_encapsulation.task.item.ItemManager;
import taskSet6_encapsulation.task.user.UserManager;

public class OnlineShop {
    public static void main(String[] args) {

        UserManager creator = new UserManager();
        creator.createUser1("Tomasz", "Nowak", 25);
        System.out.println(creator.getUser1());

        ItemManager newItem = new ItemManager();
        newItem.createShopItem("LEGO Car", "A LEGO set of bricks", 25.99);
        System.out.println(newItem.getItem1());
    }
}
