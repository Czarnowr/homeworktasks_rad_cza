package inProgress;

public class Slide97Tasks {

    public static void main(String[] args) {

    // Task 1 //

        System.out.println("Task 1:");
        System.out.println("");

        System.out.println("Default sentence: ");
        String defaultSentence1 = "Radek can program in Java";
        System.out.println(defaultSentence1);
        System.out.println("");

        System.out.println("Upper Case: ");
        String example1 = "Radek can program in Java".toUpperCase();
        System.out.println(example1);
        System.out.println("");

        System.out.println("Lower Case: ");
        String example2 = "Radek can program in Java".toLowerCase();
        System.out.println(example2);
        System.out.println("");

        System.out.println("Concatenation: ");
        String example3 = "Radek can program in Java".concat(" and C++");
        System.out.println(example3);
        System.out.println("");

        System.out.println("Subtract beginning: ");
        String example4 = "Radek can program in Java".substring(10);
        System.out.println(example4);
        System.out.println("");

        System.out.println("Subtract end: ");
        String example5 = "Radek can program in Java".substring(0, 10);
        System.out.println(example5);
        System.out.println("");

        System.out.println("Replace Word: ");
        String example6 = "Radek can program in Java".replace("Radek", "Paweł");
        System.out.println(example6);
        System.out.println("");

        System.out.println("Trim spaces before or after: ");
        String example7 = "Radek can program in Java".trim();
        System.out.println(example7);
        System.out.println("");

        System.out.println("Character number: ");
        int example8 = "Radek can program in Java".length();
        System.out.println(example8);
        System.out.println("");

    // Task 2 //


        System.out.println("Task 2:");
        System.out.println("");
        String task2 = "Simon says:".concat(" Learn your code!");
        System.out.println(task2);
        System.out.println("");

    // Task 3 //

        System.out.println("Task 3:");
        System.out.println("");
        System.out.println("   ThReE SpAcEs BeFoRe AnD AfTeR   ");
        String task3 = "   ThReE SpAcEs BeFoRe AnD AfTeR   ";
        System.out.println(task3.toLowerCase().trim());
        System.out.println("");

    // Task 4 // in Person and Family classes


    }
}
